package view;

import java.util.Scanner;

import controller.Controller;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		int option;
		while(!fin)
		{
			//Muestra cual fuente de datos va a cargar
			printMenu();
			option = linea.nextInt();
			switch(option)
			{
			case 1:
				Controller.loadGraph();
				System.out.println("Numero de vertices: " + Controller.V());
				System.out.println("Numero de arcos: " + Controller.E());
				break;
			case 2:
				Controller.loadStations();
				System.out.println("Numero de vertices: " + Controller.V());
				System.out.println("Numero de arcos: " + Controller.E());
				break;
			case 3:
				Controller.createJSON();
				break;
			case 4:
				Controller.readJSON();
				break;
			case 5: 
				Controller.loadMap();
				break;
			case 6: //Salir
				fin = true;
				linea.close();
				break;
			}
		}
	}



	private static void printMenu() {
		System.out.println("-----------------ISIS 1206 - Estructuras de Datos------======----");
		System.out.println("-------------------- taller 8 - 2018-2 ----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Cargar datos de los archivos de texto");
		System.out.println("2. Cargar estaciones al grafo");
		System.out.println("3. Persistir grafo");
		System.out.println("4. Cargar de archivo persistido");
		System.out.println("5. Dibujar mapa en google maps");
		System.out.println("6. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");
	}
}

