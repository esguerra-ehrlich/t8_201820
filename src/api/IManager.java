package api;

public interface IManager {

	void loadStations(String stationsFile);
	
	void loadIntersections();
	
	void loadGraph();
	
	int V();
	
	int E();

	void createJSON();

	void readJSON();

	void loadMap();
}