package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.data_structures.Graph;
import model.data_structures.LinkedList;

public class GraphTest {
	
	private Graph<Integer , String , Double> g;
	
	public void setupG1(){
		g = new Graph<Integer , String , Double>(100);
		g.addVertex(1, "A");
		g.addVertex(2, "B");
		g.addVertex(3, "C");
		g.addVertex(4, "D");
		g.addVertex(5, "E");
		
		g.addEdge(1, 2, 0.5);
		g.addEdge(1, 5, 100.0);
		g.addEdge(2, 3, 1.2);
		g.addEdge(2, 4, 3.3);
		g.addEdge(3, 4, 40.01);
		g.addEdge(3, 5, 2.0);
		g.addEdge(4, 5, 0.01);
		g.addEdge(2, 1, 0.5);
		g.addEdge(5, 1, 0.5);
		g.addEdge(3, 2, 0.5);
		g.addEdge(4, 2, 0.5);
	}
	
	@Test
	public void test1(){
		setupG1();
		
		LinkedList<Integer> arr = (LinkedList<Integer>) g.adj(3);
		assertTrue(arr.size() == 3);
		assertTrue(g.getInfoVertex(arr.getElement(0).getItem()).equals("D"));
		assertTrue(g.getInfoVertex(arr.getElement(1).getItem()).equals("E"));
		assertTrue(g.getInfoVertex(arr.getElement(2).getItem()).equals("B"));
		assertTrue(g.getInfoArc(3, 4) == 40.01);
		assertTrue(g.getInfoArc(3, 5) == 2.0);
		assertTrue(g.getInfoArc(3, 2) == 0.5);
		
		g.setInfoArc(3, 2, 1.2);
		assertTrue(g.getInfoArc(3, 2) == 1.2);
		g.setInfoVertex(1, "Z");
		assertTrue(g.getInfoVertex(1).equals("Z"));
	}
}
