package model.vo;

public class Vertex {
		
	private int id;
	
	private double lat;
	
	private double longi;
	
	private boolean isStation;

	public Vertex(int id , double lat, double longi, boolean isStation) {
		this.id = id;
		this.lat = lat;
		this.longi = longi;
		this.isStation = isStation;
	}
	
	public int getId(){
		return id;
	}
	
	/**
	 * @return the lat
	 */
	public double getLat() {
		return lat;
	}

	/**
	 * @return the longi
	 */
	public double getLongi() {
		return longi;
	}

	/**
	 * @return the isStation
	 */
	public boolean isStation() {
		return isStation;
	}
}
