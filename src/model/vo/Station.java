package model.vo;

import java.time.LocalDateTime;


public class Station {

	private int id;

	private String name;

	private String city;

	private double latitude;

	private double longitude;

	private int dpcapacity;

	private LocalDateTime online_date;

	public Station (String[] values) {
		this.id = Integer.parseInt(values[0]);
		this.name = values[1];
		this.city = values[2];
		this.latitude = Double.parseDouble(values[3]);
		this.longitude = Double.parseDouble(values[4]);
		this.dpcapacity = Integer.parseInt(values[5]);
		String[] valores = values[6].split(" ");
		this.online_date = convertirFecha_Hora_LDT(valores[0], valores[1]); 
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCity() {
		return city;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public int getDpcapacity() {
		return dpcapacity;
	}

	public LocalDateTime getOnline_date() {
		return online_date;
	}
	
	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos);
	}
	
}
