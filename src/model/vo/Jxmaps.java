package model.vo;
import com.teamdev.jxmaps.*;
import com.teamdev.jxmaps.Point;
import com.teamdev.jxmaps.Rectangle;
import com.teamdev.jxmaps.swing.MapView;

import javax.swing.*;
import java.awt.*;
import java.io.FileReader;
import java.util.Collection;
import java.util.Iterator;


public class Jxmaps extends MapView {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Constructor
	 * @param options
	 */
	public Jxmaps(MapViewOptions options, Iterator<Vertex> grafo) {
		super(options);

		setOnMapReadyHandler(new MapReadyHandler() {			
			@Override
			public void onMapReady(MapStatus status) {
				if (status == MapStatus.MAP_STATUS_OK) {

					// defincion del mapa y posicionamiento del centro en Chicago 41.8781, -87.6298
					final Map map = getMap();
					map.setZoom(15.0);
					map.setCenter(new LatLng(41.8781, -87.6298));
					
					//---
					// if Station, dibujar rectangulo. Else dibujar circulo
					//---
					
					// Cada estacion es marcada en el mapa. El radio del circulo es determinado por la capacidad de la estacion.					
					while (grafo.hasNext()) {
						Vertex v = grafo.next();
						LatLng coords = new LatLng((v.getLat()), (v.getLongi()));
						if (v.isStation())
						{
							LatLng SE = new LatLng((v.getLat()-0.01), v.getLongi()-0.01);
							drawRectangle(map, SE, coords);
						}
						else 
						{
							drawMarker(map, coords, 20);
						}
					}
                    
					
				}
			}
		});

	}
	
	
	
	/**
	 * Grafica un pointer en el mapa en las coordenas dadas y dibuja un circulo de circunferencia dada
	 * @param mapa Mapa donde se va definir
	 * @param coords Latitude, Longitude
	 * @param diameter del circulo a dibujar al rededor de la coordenada
	 */
	private void drawMarker( Map map, LatLng coords, int diameter) {	
		Marker marker = new Marker(map);
		marker.setPosition(coords);
		
		Circle circle = new Circle(map);
		circle.setCenter(coords); 
		circle.setRadius(diameter);
	}

	
	/**
	 * Grafica un circulo en el mapa en las coordenas dadas 
	 * @param mapa Mapa donde se va definir
	 * @param coords Latitude, Longitude
	 * @param diameter del circulo a dibujar al rededor de la coordenada
	 */
	private void drawCercle( Map map, LatLng coords, int diameter) {	
		Circle circle = new Circle(map);
		circle.setCenter(coords); 
		circle.setRadius(diameter);
	}	
	/**
	 * Grafica un circulo en el mapa en las coordenas dadas 
	 * @param mapa Mapa donde se va definir
	 * @param coords Latitude, Longitude
	 * @param diameter del circulo a dibujar al rededor de la coordenada
	 */
	private void drawRectangle( Map map, LatLng SWCoords, LatLng NECoords) {	
		Rectangle rect = new Rectangle(map);
		RectangleOptions r = new RectangleOptions();
		LatLngBounds x = new LatLngBounds(SWCoords, NECoords);
		r.setBounds(x);
		rect.setOptions(r);
		
	}	
	/**
	 * Grafica un conjunto de coordenadas y las une mediante rectas para generar una ruta
	 * @param mapa Mapa donde se va definir
	 * @param path Array of LatLng
	 */
	private void polylinePath(Map map, LatLng[] path) {
        Polyline polyline = new Polyline(map);
        polyline.setPath(path);

        PolylineOptions options = new PolylineOptions();
        options.setGeodesic(true);
        options.setStrokeColor("#00aafd");
        options.setStrokeOpacity(1.0);
        polyline.setOptions(options);
	}
	

	/**
	 * Calcula y grafica las direcciones para dos localizaciones dadas
	 * @param origin direccion de partida e.g: Desplaines St & Jackson Blvd
	 * @param destination direccion de llegada e.g: Desplaines St & Kinzie St
	 */
	private void calculateAddressDirections(String origin, String destination) {
        final Map map = getMap();
        DirectionsRequest request = new DirectionsRequest();
       
        request.setOriginString(origin);
        request.setDestinationString(destination);
        
        request.setTravelMode(TravelMode.DRIVING);
        getServices().getDirectionService().route(request, new DirectionsRouteCallback(map) {
            @Override
            public void onRoute(DirectionsResult result, DirectionsStatus status) {
                if (status == DirectionsStatus.OK) {
                    map.getDirectionsRenderer().setDirections(result);
                } else {
                    System.out.println("Error. Route cannot be calculated.\nPlease correct input data.");
                }
            }
        });
    }
	

	
	/**
	 * Calcula y grafica las direcciones para dos conjuntos de coordenadas dadas
	 * @param origin
	 * @param destination
	 */
	private void calculateCoordsDirections(LatLng origin, LatLng destination) {
        final Map map = getMap();
        DirectionsRequest request = new DirectionsRequest();
       
        request.setOrigin(origin);
        request.setDestination(destination);
        
        request.setTravelMode(TravelMode.DRIVING);
        getServices().getDirectionService().route(request, new DirectionsRouteCallback(map) {
            @Override
            public void onRoute(DirectionsResult result, DirectionsStatus status) {
                if (status == DirectionsStatus.OK) {
                    map.getDirectionsRenderer().setDirections(result);
                } else {
                    System.out.println("Error. Route cannot be calculated.\nPlease correct input data.");
                }
            }
        });
    }




	
	
	


	



	
	
	


}