package model.data_structures;

public interface IPriorityQueue<T>
{
	//void MaxPQ(); //create a priority queue
	//void MaxPQ(int max) //create a priority queue of initial capacity max
	//MaxPQ(Key[] a) //create a priority queue from the keys in a[]
	void insert(T v); //insert a key into the priority queue
	T max(); //return the largest key
	T delMax(); //return and remove the largest key
	boolean isEmpty(); //is the priority queue empty?
	int size(); //number of keys in the priority queue
}
