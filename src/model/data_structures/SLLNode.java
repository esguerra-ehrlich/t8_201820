package model.data_structures;


public class SLLNode<K extends Comparable<K>, V> {

	// linked-list node
	public K key;
	public V val;
	public SLLNode<K,V> next;
	public SLLNode(K key, V val, SLLNode<K,V> next)
	{
		this.key = key;
		this.val = val;
		this.next = next;
	}
	

}
