package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements Iterable<T>{

	private SinglyNode<T> head;
	
	private SinglyNode<T> tail;
	
	private int size;
	

	public LinkedList(T first){
		head = new SinglyNode<T>(first , null);
		size = 1;
		tail = head;
	}
	
	public LinkedList(){
		size = 0;
		head = null;
		tail = null;
	}


	public Integer size() {
		return size;
	}

	public void addAtBeginning(T ele) {
		if(head == null){
			head = new SinglyNode<T>(ele , null);
			tail = head;
		}
		else{
			SinglyNode<T> temp = head;
			head = new SinglyNode<T>(ele , temp);
		}
		size++;
	}


	public void addAtEnd(T ele) {
		if(tail == null){
			tail = new SinglyNode<T>(ele , null);
			head = tail;
		}
		else{
			SinglyNode<T> temp = tail;
			tail = new SinglyNode<T>(ele , null);
			temp.setNext(tail);
		}
		size++;
	}


	public void addAtK(int k, T ele) {
		if(k > size || k < 0)return;
		if(k == size - 1){
			addAtEnd(ele);
			return;
		}
		if(k == 0){
			addAtBeginning(ele);
			return;
		}
		
		SinglyNode<T> x = getElement(k - 1) , nuevo = new SinglyNode<T>(ele , x);
		nuevo.setNext(x.getNext());
		x.setNext(nuevo);
		size++;
	}

	
	public SinglyNode<T> getElement(int i) {
		SinglyNode<T> curr = head;
		while(i-- != 0 && curr != null)
			curr = curr.getNext();
		return curr;
	}

	
	public void deleteLast() {
		if(tail == null)  return;
		if(size == 1){
			head = tail = null;
			return;
		}
		
		tail = getElement(size-2);
		tail.setNext(null);
		size--;
	}	
	
	public void deleteFirst(){
		if(head == null)return;
		head = head.getNext();
		if(head == null)
			tail = null;
		size--;
	}

	
	public Iterator<T> iterator() {
		return new Iterator<T>(){
			private SinglyNode<T> curr = head;
			
			@Override
			public boolean hasNext() {
				return curr != null;
			}

			@Override
			public T next() {
				T ans = curr.getValue();
				curr = curr.getNext();
				return ans;
			}
		};
	}

	public void add(T ele) {
		addAtEnd(ele);
	}
}
