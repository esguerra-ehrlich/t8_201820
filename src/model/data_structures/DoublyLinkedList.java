package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T>{

	private Node<T> head;

	private Node<T> tail;

	private int size;


	public DoublyLinkedList(T first){
		head = new Node<T>(null , first , null);
		size = 1;
		tail = head;
	}

	public DoublyLinkedList(){
		size = 0;
		head = null;
		tail = null;
	}

	@Override
	public Integer getSize() {
		return size;
	}

	@Override
	public void addAtBeginning(T ele) {
		if(head == null){
			head = new Node<T>(null , ele , null);
			tail = head;
		}
		else{
			Node<T> temp = head;
			head = new Node<T>(null , ele , temp);
			temp.setPrev(head);
		}
		size++;
	}

	@Override
	public void addAtEnd(T ele) {
		if(tail == null){
			tail = new Node<T>(null , ele , null);
			head = tail;
		}
		else{
			Node<T> temp = tail;
			tail = new Node<T>(temp , ele , null);
			temp.setNext(tail);
		}
		size++;
	}

	@Override
	public void addAtK(int k, T ele) {
		if(k > size || k < 0)return;
		if(k == size){
			addAtEnd(ele);
			return;
		}
		if(k == 0){
			addAtBeginning(ele);
			return;
		}

		Node<T> x = getNode(k) , nuevo = new Node<T>(x.getPrev() , ele , x);
		x.getPrev().setNext(nuevo);
		x.setPrev(nuevo);
		size++;
	}


	@Override
	public Node<T> getNode(int i) {
		Node<T> curr = head;
		while(i-- != 0 && curr != null){
			curr = curr.getNext();
		}
		return curr;
	}

	@Override
	public T getElement(int i) {
		return getNode(i).getItem();
	}

	@Override
	public void deleteLast() {
		if(tail != null){
			if(tail.getPrev() != null){
				tail.getPrev().setNext(null);
				tail = tail.getPrev();
			}
			else
				tail = head = null;
			size--;
		}
	}

	@Override
	public void deleteAtK(int k) {
		if(k > size)return;
		if(k == size){
			deleteLast();
			return;
		}
		if(k == 0){
			deleteFirst();
			return;
		}

		Node<T> x = getNode(k);
		x.getPrev().setNext(x.getNext());
		x.getNext().setPrev(x.getPrev());
		size--;
	}

	@Override
	public void deleteFirst(){
		if(head == null)return;
		head = head.getNext();
		if(head == null)
			tail = null;
		else{
			head.setPrev(null);
		}
		size--;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>(){
			private Node<T> curr = head;

			@Override
			public boolean hasNext() {
				return curr != null;
			}

			@Override
			public T next() {
				T ans = curr.getItem();
				curr = curr.getNext();
				return ans;
			}

			@Override
			public void remove(){
				if(curr.getPrev().equals(head))
					deleteFirst();
				else{
					Node<T> pre = curr.getPrev().getPrev();
					if(pre != null)
						pre.setNext(curr);
					curr.setPrev(pre);
					size--;					
				}
			}
		};
	}

	public void add(T deserialize) {
		addAtEnd(deserialize);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}


	@Override
	public T get(int pos) {
		return getElement(pos);
	}
}
