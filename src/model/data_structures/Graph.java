package model.data_structures;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Iterator;

public class Graph<K extends Comparable<K> , V , A> {
	
	private LinearProbingHT<K , Integer > ht;
	
	private V[] vertex;
	
	private  LinkedList<Edge>[] g;
	
	private int v;
	
	private int e;
	
	/**
	 * Create a graph, with expected final amount of vertices size. The amount of vertices can surpass size but it will not be as efficient. 
	 * @param size expected amount of vertices of the graph.
	 */
	@SuppressWarnings("unchecked")
	public Graph(int size){
		ht = new LinearProbingHT<>(BigInteger.valueOf(size*2).nextProbablePrime().intValue());
		vertex = (V[]) new Object[size + 1];
		g = new LinkedList[size + 1];
		v = 0;
		e = 0;
	}
	
	public int V(){
		return v;
	}
	
	public int E(){
		return e;
	}
	
	public void addVertex( K id , V info){
		ht.put(id, ++v);
		if(v >= g.length){
			vertex = Arrays.copyOf(vertex, 2*v);
			g = Arrays.copyOf(g, 2*v);
		}
		vertex[ht.get(id)] = info;
		g[ht.get(id)] = new LinkedList<>();
	}

	public void addEdge(K idV , K idU , A info){
		Edge edge = new Edge(idV , idU , info);
		g[ht.get(idV)].add(edge);
		e++;
	}
	
	public V getInfoVertex(K id){
		return vertex[ht.get(id)];
	}
	
	public void setInfoVertex(K id , V info){
		vertex[ht.get(id)] = info;
	}
	
	public A getInfoArc(K idI , K idf){
		for(Edge edge : g[ht.get(idI)])
			if(edge.idf.equals(idf))
				return edge.w;
		return null;
	}
	
	public void setInfoArc(K idI , K idF , A info){
		for(Edge edge : g[ht.get(idI)])
			if(edge.idf.equals(idF)){
				edge.w = info;
				break;
			}
	}
	
	public Iterable<K> adj(K idVertex){
		LinkedList<K> ans = new LinkedList<>();
		for(Edge edge : g[ht.get(idVertex)])
			ans.addAtEnd(edge.idf);
		return ans;
	}
	
	public Iterable<Edge> adjEdge(K idVertex){
		LinkedList<Edge> ans = new LinkedList<>();
		for(Edge edge : g[ht.get(idVertex)])
			ans.addAtEnd(edge);
		return ans;
	}
	
	public Iterator<V> allvertex(){
		LinkedList<V> arr = new LinkedList<>();
		for(V ele : vertex)
			if(ele != null) arr.addAtEnd(ele);
		return arr.iterator();
	}

	public class Edge{
		public K idi;
		public K idf;
		public A w;
		public Edge(K pida , K pidb , A pw){idi = pida; idf = pidb; w = pw;}
	}
}
