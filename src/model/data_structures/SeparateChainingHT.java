package model.data_structures;

import java.util.Iterator;

/**
 * HashTable que resuelve conflictos con Separate Chaining
 * @param <K> Las llaves, deben ser comparables
 * @param <V> Los valores.
 */
@SuppressWarnings("rawtypes")
public class SeparateChainingHT<K extends Comparable<K>, V> implements Iterable<V>
{
	/**
	 * Max Loading Factor allowed
	 */
	private int f;

	/**
	 * Elements in the hash table
	 */
	private int n;
	/**
	 * Size of the hash-table
	 */
	private int m;

	/**
	 * Auxiliary Array of SequentialSearchSymbolTable
	 */
	private SequentialSearchST<K,V>[] st;
	/**
	 * Creates an instance of this class
	 * @param initialCapacity the initial size of the hashtable
	 * @param loadingFactor, the maximum size allowed for each linkedList inside the array
	 */
	private int[] primes = {31,61,127,251,509,1021,2039,4093,8191,16381,32749,65521,131071,262139,524287,1048573,2097143,4194301, };
	@SuppressWarnings("unchecked")
	public SeparateChainingHT(int initialCapacity, int loadingFactor)
	{
		n = 0;
		f = loadingFactor;
		m = nextPrime(initialCapacity);
		st = (SequentialSearchST<K, V>[]) new SequentialSearchST[m];
		for (int i = 0; i < m; i++)
			st[i] = new SequentialSearchST();
	}
	/**
	 * Returns the immediately greater prime than x
	 */
	private int nextPrime(int x) {
		int i = 0;
		int prime = primes[i];
		while (prime < x) 
		{
			i++;
			prime = primes[i];
		}
		return prime;
	}

	private int hash(K key)
	{ 
		return (key.hashCode() & 0x7fffffff) % m; 
	}

	public V get(K key)
	{
		return (V) st[hash(key)].get(key); 
	}

	public V delete(K key)
	{
		V temp = (V) st[hash(key)].delete(key);
		if (n/m <= f/5)  // por si es muy chiquita
		{
			int neu = nextPrime(n/2);
			resize(neu);
		}
		if (temp != null)
			n--;
		return temp;
	}

	public void put(K key, V val)
	{ 
		st[hash(key)].put(key, val);
		n++;
		if (n/m >= f)
		{
			int neu = nextPrime(2*n);
			resize(neu);
		}

	}

	private void resize(int newCap)
	{
		SeparateChainingHT<K, V> t;
		t = new SeparateChainingHT<K, V>(newCap, f);
		for (int i = 0; i < m; i++)
			if (st[i] != null)
			{
				for (SLLNode<K,V> x = st[i].getFirst(); x != null; x = x.next )
				{
					t.put(x.key, x.val);
				}
			}
		n = t.n;
		st = t.st;
		m = t.m;
	}
	
	public SequentialSearchST<K,V> getAllFromKey(K key)
	{
		return st[hash(key)];
	}

	public Iterator<V> iterator() {
		return new Iterator<V>(){
			private int i = 0;
			private SLLNode x = null;

			@Override
			public boolean hasNext() {
				if(x == null)
					while(++i < m && x == null) x = st[i].getFirst();
				return x != null;
			}

			@Override
			public V next() {
				@SuppressWarnings("unchecked")
				V val = (V) x.val;
				x = x.next;
				return val;
			}
		};
	}

}
