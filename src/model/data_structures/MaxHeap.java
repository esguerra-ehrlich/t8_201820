package model.data_structures;

import java.util.NoSuchElementException;

public class MaxHeap <T extends Comparable<T> >{
	
	private T[] pq;
	
	private int n;
	
	@SuppressWarnings("unchecked")
	public MaxHeap(int max){
		n = 0;
		pq =  (T[]) new Comparable[max + 1];
	}
	
	public int getSize(){
		return n;
	}
	
	public void add(T ele){
		pq[++n] = ele;
		swim(n);
	}
	
	public T max(){
		if(n == 0)throw new NoSuchElementException("Stack underflow");
		T ele = pq[1];
		pq[1] = pq[n];
		pq[n--] = null;
		sink(1);
		return ele;
	}
	
	public boolean isEmpty(){
		return n == 0;
	}
	
	public int maxSize(){
		return pq.length - 1;
	}

	private void sink(int i) {
		while((i <<= 1) <= n){
			if(i < n && pq[i].compareTo(pq[(i|1)]) < 0) i|=1;
			if(pq[i>>1].compareTo(pq[i]) >= 0)break;
			T temp = pq[i>>1];
			pq[i>>1] = pq[i];
			pq[i] = temp;
		}
	}

	private void swim(int i) {
		while(i != 1 && pq[i].compareTo(pq[i>>1]) > 0){
			T temp = pq[i];
			pq[i] = pq[i>>1];
			pq[i>>1] = temp;
			i = i>>1;
		}
	}
}
