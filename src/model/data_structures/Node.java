package model.data_structures;

public class Node<T> {
	
	private T value;
	
	private Node<T> next;
	
	private Node<T> prev;
	
	public Node( Node<T> pprev , T pval , Node<T> pnext){
		value = pval;
		next = pnext;
		prev = pprev;
	}

	/**
	 * @return the value
	 */
	public T getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(T value) {
		this.value = value;
	}

	/**
	 * @return the next
	 */
	public Node<T> getNext() {
		return next;
	}

	/**
	 * @return the prev
	 */
	public Node<T> getPrev() {
		return prev;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext(Node<T> next) {
		this.next = next;
	}

	/**
	 * @param prev the prev to set
	 */
	public void setPrev(Node<T> prev) {
		this.prev = prev;
	}

	public T getItem() {
		return value;
	}
}
