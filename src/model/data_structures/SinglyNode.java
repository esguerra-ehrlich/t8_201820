package model.data_structures;

public class SinglyNode<T> {

	private T value;
	
	private SinglyNode<T> next;
	
	
	public SinglyNode( T pval , SinglyNode<T> pnext){
		value = pval;
		next = pnext;
	}

	/**
	 * @return the value
	 */
	public T getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(T value) {
		this.value = value;
	}

	/**
	 * @return the next
	 */
	public SinglyNode<T> getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext(SinglyNode<T> next) {
		this.next = next;
	}

	public T getItem() {
		return value;
	}
}
