package model.data_structures;


import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * Based on algorithms 4th edition Sedgewick Tree implementation.
 * @param <K> A type instance that has to be comparable, since keys in a RBT are orderable. 
 * @param <V> Values corresponding to each key. 
 */
public class RedBlackBST<K extends Comparable<K>, V> {

	/**
	 * Constants to improve code readability. 
	 */
	private static final boolean RED   = true;
	private static final boolean BLACK = false;

	/**
	 * The root of the tree.
	 */
	public RBTNode root;

	// BST helper node data type
	private class RBTNode {
		private K key;           
		private V value;         
		private RBTNode left, right; 
		private boolean color;     // true = red, false = black.
		private int size;          

		public RBTNode(K pKey, V pVal, boolean pColor, int pSize) {
			key = pKey;
			value = pVal;
			color = pColor;
			size = pSize;
		}
	}

	/**
	 * Default initializing method
	 */
	public RedBlackBST() 
	{

	}

	/***************************************************************************
	 *  Node helper methods.
	 ***************************************************************************/
	private boolean isRed(RBTNode x) {
		if (x == null) return false;
		return x.color == RED;
	}

	private int size(RBTNode x) {
		if (x == null) return 0;
		return x.size;
	} 


	public int size() {
		return size(root);
	}


	public boolean isEmpty() {
		return root == null;
	}

	/**
	 * Returns the value associated with the given key.
	 * @param key the key.
	 * @return the value associated with the given key, null if key == null or not found.
	 * @throws IllegalArgumentException if the key is null.
	 */
	public V get(K key) {
		if (key == null) throw new IllegalArgumentException("argument to get() is null");
		return get(root, key);
	}

	public int distance(RBTNode x, K key) {
		int d = 0;
		while (x != null) {
			int cmp = key.compareTo(x.key);
			if      (cmp < 0)
			{
				x = x.left;
				d++;
			}
			else if (cmp > 0)
			{
				x = x.right;
				d++;
			}
			else
				return ++d;
		}
		return d;
	}

	private V get(RBTNode x, K key) {
		while (x != null) {
			int cmp = key.compareTo(x.key);
			if      (cmp < 0) x = x.left;
			else if (cmp > 0) x = x.right;
			else              return x.value;
		}
		return null;
	}

	public boolean contains(K key) {
		return get(key) != null;
	}

	/***************************************************************************
	 *  Red-black tree methods
	 ***************************************************************************/

	/**
	 * Inserts the specified key-value pair into the symbol table, overwriting the old 
	 * value with the new value if the symbol table already contains the specified key.
	 * If value == null, deletes the corresponding key from the RBT.
	 *
	 * @param key the key
	 * @param val the value
	 * @throws IllegalArgumentException if key == null.
	 */
	public void put(K key, V val) {
		if (key == null) throw new IllegalArgumentException("first argument to put() is null");
		if (val == null) {
			delete(key);
			return;
		}

		root = put(root, key, val);
		root.color = BLACK;   // Always change root back to black!
	}

	// insert the key-value pair in the subtree rooted at h
	private RBTNode put(RBTNode h, K key, V val) { 
		if (h == null) return new RBTNode(key, val, RED, 1);

		// BST insertion.
		int cmp = key.compareTo(h.key);
		if      (cmp < 0) h.left  = put(h.left,  key, val); 
		else if (cmp > 0) h.right = put(h.right, key, val); 
		else              h.value   = val;

		// fix-up any right-leaning links
		if (isRed(h.right) && !isRed(h.left))      h = rotateLeft(h);
		if (isRed(h.left)  &&  isRed(h.left.left)) h = rotateRight(h);
		if (isRed(h.left)  &&  isRed(h.right))     flipColors(h);

		// Size of a subtree is always the size of its subtrees plus the parent node.
		h.size = size(h.left) + size(h.right) + 1;

		return h;
	}


	/**
	 * Removes the smallest key and associated value from the symbol table.
	 * @throws NoSuchElementException if the symbol table is empty
	 */
	public void deleteMin() {
		if (isEmpty()) throw new NoSuchElementException("BST underflow");

		// if both children of root are black, set root to red
		if (!isRed(root.left) && !isRed(root.right))
			root.color = RED;

		root = deleteMin(root);
		if (!isEmpty()) root.color = BLACK;

	}

	private RBTNode deleteMin(RBTNode h) { 
		if (h.left == null)
			return null;

		if (!isRed(h.left) && !isRed(h.left.left))
			h = moveRedLeft(h);

		h.left = deleteMin(h.left);
		return balance(h);
	}

	public void deleteMax() {
		if (isEmpty()) throw new NoSuchElementException("BST underflow");

		if (!isRed(root.left) && !isRed(root.right))
			root.color = RED;

		root = deleteMax(root);
		if (!isEmpty()) root.color = BLACK;
		// assert check();
	}


	private RBTNode deleteMax(RBTNode h) { 
		if (isRed(h.left))
			h = rotateRight(h);

		if (h.right == null)
			return null;

		if (!isRed(h.right) && !isRed(h.right.left))
			h = moveRedRight(h);

		h.right = deleteMax(h.right);

		return balance(h);
	}

	/**
	 * Removes the specified key and its associated value from this symbol table     
	 * (if the key is in this symbol table).    
	 *
	 * @param  key the key
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public void delete(K key) { 
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");
		if (!contains(key)) return;

		// if both children of root are black, set root to red
		if (!isRed(root.left) && !isRed(root.right))
			root.color = RED;

		root = delete(root, key);
		if (!isEmpty()) root.color = BLACK;
	}

	private RBTNode delete(RBTNode h, K key) { 


		if (key.compareTo(h.key) < 0)  {
			if (!isRed(h.left) && !isRed(h.left.left))
				h = moveRedLeft(h);
			h.left = delete(h.left, key);
		}
		else {
			if (isRed(h.left))
				h = rotateRight(h);
			if (key.compareTo(h.key) == 0 && (h.right == null))
				return null;
			if (!isRed(h.right) && !isRed(h.right.left))
				h = moveRedRight(h);
			if (key.compareTo(h.key) == 0) {
				RBTNode x = min(h.right);
				h.key = x.key;
				h.value = x.value;

				h.right = deleteMin(h.right);
			}
			else h.right = delete(h.right, key);
		}
		return balance(h);
	}



	public int height() {
		return height(root);
	}
	private int height(RBTNode x) {
		if (x == null) return -1;
		return 1 + Math.max(height(x.left), height(x.right));
	}


	public K min() {
		if (isEmpty()) throw new NoSuchElementException("calls min() with empty symbol table");
		return min(root).key;
	} 


	private RBTNode min(RBTNode x) { 

		if (x.left == null) return x; 
		else                return min(x.left); 
	} 


	public K max() {
		if (isEmpty()) throw new NoSuchElementException("calls max() with empty symbol table");
		return max(root).key;
	} 


	private RBTNode max(RBTNode x) { 

		if (x.right == null) return x; 
		else                 return max(x.right); 
	} 



	public K floor(K key) {
		if (key == null) throw new IllegalArgumentException("argument to floor() is null");
		if (isEmpty()) throw new NoSuchElementException("calls floor() with empty symbol table");
		RBTNode x = floor(root, key);
		if (x == null) return null;
		else           return x.key;
	}    

	private RBTNode floor(RBTNode x, K key) {
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0) return x;
		if (cmp < 0)  return floor(x.left, key);
		RBTNode t = floor(x.right, key);
		if (t != null) return t; 
		else           return x;
	}


	public K ceiling(K key) {
		if (key == null) throw new IllegalArgumentException("argument to ceiling() is null");
		if (isEmpty()) throw new NoSuchElementException("calls ceiling() with empty symbol table");
		RBTNode x = ceiling(root, key);
		if (x == null) return null;
		else           return x.key;  
	}
	private RBTNode rotateRight(RBTNode h) {
		RBTNode x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = x.right.color;
		x.right.color = RED;
		x.size = h.size;
		h.size = size(h.left) + size(h.right) + 1;
		return x;
	}


	private RBTNode rotateLeft(RBTNode h) {

		RBTNode x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = x.left.color;
		x.left.color = RED;
		x.size = h.size;
		h.size = size(h.left) + size(h.right) + 1;
		return x;
	}

	private void flipColors(RBTNode h) {
		h.color = !h.color;
		h.left.color = !h.left.color;
		h.right.color = !h.right.color;
	}


	private RBTNode moveRedLeft(RBTNode h) {


		flipColors(h);
		if (isRed(h.right.left)) { 
			h.right = rotateRight(h.right);
			h = rotateLeft(h);
			flipColors(h);
		}
		return h;
	}


	private RBTNode moveRedRight(RBTNode h) {
		flipColors(h);
		if (isRed(h.left.left)) { 
			h = rotateRight(h);
			flipColors(h);
		}
		return h;
	}

	/**
	 * Fixes the broken conditions for the red black tree.
	 * @param h The node to evaluate its correctness.
	 * @return the fixed node.
	 */
	private RBTNode balance(RBTNode h) {
		// assert (h != null);

		if (isRed(h.right))                      h = rotateLeft(h);
		if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if (isRed(h.left) && isRed(h.right))     flipColors(h);

		h.size = size(h.left) + size(h.right) + 1;
		return h;
	}






	private RBTNode ceiling(RBTNode x, K key) {  
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0) return x;
		if (cmp > 0)  return ceiling(x.right, key);
		RBTNode t = ceiling(x.left, key);
		if (t != null) return t; 
		else           return x;
	}


	public K select(int k) {
		if (k < 0 || k >= size()) {
			throw new IllegalArgumentException("argument to select() is invalid: " + k);
		}
		RBTNode x = select(root, k);
		return x.key;
	}

	private RBTNode select(RBTNode x, int k) {
		int t = size(x.left); 
		if      (t > k) return select(x.left,  k); 
		else if (t < k) return select(x.right, k-t-1); 
		else            return x; 
	} 

	public int rank(K key) {
		if (key == null) throw new IllegalArgumentException("argument to rank() is null");
		return rank(key, root);
	} 

	private int rank(K key, RBTNode x) {
		if (x == null) return 0; 
		int cmp = key.compareTo(x.key); 
		if      (cmp < 0) return rank(key, x.left); 
		else if (cmp > 0) return 1 + size(x.left) + rank(key, x.right); 
		else              return size(x.left); 
	} 


	public Iterator<K> keys() {
		if (isEmpty()) return new Queue<K>().iterator();
		return keysInRange(min(), max());
	}

	public Iterator<K> keysInRange(K lo, K hi) {
		if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
		if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

		Queue<K> queue = new Queue<K>();
		keys(root, queue, lo, hi);
		return queue.iterator();
	}

	public Iterator<V> valuesInRange(K lo, K hi) {
		if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
		if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

		Queue<V> queue = new Queue<V>();
		vals(root, queue, lo, hi);
		return queue.iterator();
	}

	private void vals(RBTNode x, Queue<V> queue, K lo, K hi) { 
		if (x == null) return; 
		int cmplo = lo.compareTo(x.key); 
		int cmphi = hi.compareTo(x.key); 
		if (cmplo < 0) vals(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.value);
		if (cmphi > 0) vals(x.right, queue, lo, hi); 
	}

	private void keys(RBTNode x, Queue<K> queue, K lo, K hi) { 
		if (x == null) return; 
		int cmplo = lo.compareTo(x.key); 
		int cmphi = hi.compareTo(x.key); 
		if (cmplo < 0) keys(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
		if (cmphi > 0) keys(x.right, queue, lo, hi); 
	}

	public int size(K lo, K hi) {
		if (lo == null) throw new IllegalArgumentException("first argument to size() is null");
		if (hi == null) throw new IllegalArgumentException("second argument to size() is null");

		if (lo.compareTo(hi) > 0) return 0;
		if (contains(hi)) return rank(hi) - rank(lo) + 1;
		else              return rank(hi) - rank(lo);
	}

}
