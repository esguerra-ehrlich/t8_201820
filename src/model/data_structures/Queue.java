package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<T> implements Iterable<T>, IQueue<T>{

	private Node<T> head;
	
	private int size;
	
	private Node<T> tail;
	
	public Queue(){
		head = tail = null;
		size = 0; 
	}
	


	public boolean isEmpty() {
		return size == 0;
	}


	public int size() {
		return size;
	}


	public void enqueue(T t) {
		if(tail == null)
			head = tail = new Node<T>(null , t , null);
		else{
			Node<T> temp = tail;
			tail = new Node<T>(temp , t , null);
			temp.setNext(tail);
		}
		size++;
	}

	public T dequeue() {
		if(head == null)
			throw new NoSuchElementException("Stack underflow");

		Node<T> temp = head;
		head = temp.getNext();
		if(size == 1)
			tail = null;
		size--;
		return temp.getItem();
	}

	public Iterator<T> iterator() {
		return new Iterator<T>(){
			private Node<T> curr = head;
			
			@Override
			public boolean hasNext() {
				return curr != null;
			}

			@Override
			public T next() {
				T ans = curr.getItem();
				curr = curr.getNext();
				return ans;
			}
		};
	}
}
