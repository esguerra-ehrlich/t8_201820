package model.data_structures;

import model.data_structures.SLLNode;

public class SequentialSearchST<K extends Comparable<K>, V>
{
	private SLLNode<K,V> first; // first node in the linked list
	public int size;
	
	public V get(K key)
	{
		for (SLLNode<K,V> x = first; x != null; x = x.next)
			if (key.equals(x.key))
				return x.val; 
		return null; 
	}
	public void put(K key, V val)	
	{ 
		for (SLLNode<K,V> x = first; x != null; x = x.next)
			if (key.equals(x.key))
			{
				x.val = val;  
				return;
			} 
		first = new SLLNode<K,V>(key, val, first); 
		size++;
	}
	public SLLNode<K,V> getFirst() 
	{
		return first;
	}
	public V delete(K key)
	{ 
		V temp = null;
		for (SLLNode<K,V> x = first; x != null && temp != null; x = x.next)
			if (key.equals(x.key))
			{
				temp = x.val; 
				x = x.next;  
				size--;
			}	
		return temp;
	}
} 