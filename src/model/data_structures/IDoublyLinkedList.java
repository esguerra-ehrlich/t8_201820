package model.data_structures;
/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */

public interface IDoublyLinkedList<T> extends Iterable<T> {

	Integer getSize();
	
	void addAtBeginning(T ele);
	
	void addAtEnd(T ele);
	
	void addAtK(int k , T ele);
	
	T getElement(int i);
		
	void deleteLast();
	
	void deleteAtK(int k);
	
	void deleteFirst();

	Node<T> getNode(int i);

	T get(int pos);

	boolean isEmpty();

	int size();
}
