package model.data_structures;

import java.math.BigInteger;
import java.util.Iterator;

public class LinearProbingHT<K extends Comparable<K>, V> implements Iterable<K>{

	private int N;
	
	private int M;
	
	private K[] keys;
	
	private V[] vals;

	private double alpha = 0.5;
	
	@SuppressWarnings("unchecked")
	public LinearProbingHT(int m){
		M = m;
		N = 0;
		keys = (K[]) new Comparable[M];
		vals = (V[]) new Object[M];
	}
	
	private int hash(K key){
		return (key.hashCode() & 0x7fffffff) % M;
	}
	
	public void put(K key , V val){
		if(N >= M*alpha)rehash();
		
		int i = hash(key);
		for(; keys[i] != null ; i = (i + 1)%M)
			if(keys[i].equals(key)){
				vals[i] = val;
				return;
			}
		keys[i] = key;
		vals[i] = val;
		N++;
	}
	
	public V get(K key){
		for(int i = hash(key) ; keys[i] != null ; i = (i + 1)%M)
			if(keys[i].equals(key))
				return vals[i];
		return null;
	}
	
	public void delete(K key){
		int i = hash(key);
		while(keys[i] != null && !keys[i].equals(key)){i = (i + 1)%M;}
		if(keys[i] == null)
			return;
		
		keys[i] = null; vals[i] = null;
		K tempKey; V tempVal;
		while(keys[(i = (i + 1)%M)] != null){
			tempKey = keys[i];
			tempVal = vals[i];
			keys[i] = null; vals[i] = null; N--;
			put(tempKey , tempVal);
		}
		N--;
	}

	private void rehash() {
 		int tSize = BigInteger.valueOf(2*M).nextProbablePrime().intValue();
 		
		LinearProbingHT<K , V> temp = new LinearProbingHT<>(tSize);
		for(int i = 0 ; i < M ; ++i)
			if(keys[i] != null) temp.put(keys[i] , vals[i]);
		M = tSize;
		keys = temp.keys;
		vals = temp.vals;
	}
	

	@Override
	public Iterator<K> iterator() {
		return new Iterator<K>(){
			private int i = 0;

			@Override
			public boolean hasNext() {
				int in = i;
				while(in < M && keys[in] == null){in++;};
				return in < M;
			}

			@Override
			public K next() {
				while(i < M && keys[i] == null){i++;};
				return keys[i++];
			}
		};
	}
	
	public void setAlpha(double d){
		alpha = d;
	}
}