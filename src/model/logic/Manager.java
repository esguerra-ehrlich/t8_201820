package model.logic;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import com.google.gson.Gson;
import com.opencsv.CSVReader;
import com.teamdev.jxmaps.MapViewOptions;

import api.IManager;
import model.data_structures.Graph;
import model.data_structures.Graph.Edge;
import model.data_structures.Haversine;
import model.vo.Jxmaps;
import model.vo.Station;
import model.vo.Vertex;

public class Manager implements IManager {

	//Ruta del archivo de stations 2017-Q1-Q2
	public static final String STATIONS_Q1_Q2 = "./data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	public static final String STATIONS_Q3_Q4 = "./data/Divvy_Stations_2017_Q3Q4.csv";
	
	public static final String NODES = "./data/Nodes_of_Chicago_Street_Lines.txt";

	public static final String ADJ_LIST = "./data/Adjacency_List_of_Chicago_Street_Lines.txt";
	
	public static final String VERTEX_JSON = "./data/jsonVertex.txt";
	
	public static final String EDGE_JSON = "./data/jsonEdge.txt";
	
	private Graph<Integer , Vertex , Double> g;
	
 	public Manager() {
 		g = new Graph<>(325000);
 	}

	@Override
	public void loadStations (String stationsFile) {
		CSVReader reader;
		int stationsCounter = 0;
		try {
			reader = new CSVReader(new FileReader(stationsFile));
			String [] nextLine;
			nextLine = reader.readNext();  // to omit the headers

			while ((nextLine = reader.readNext()) != null) {
				// nextLine[] is an array of values from the line
				Station temp = new Station(nextLine);
				Vertex v = new Vertex(-temp.getId() , temp.getLatitude() , temp.getLongitude() , true);
				g.addVertex(-temp.getId(), v);
				addClosestEdge(-temp.getId() , v);
				++stationsCounter;
			}
		System.out.println("added " + stationsCounter + " stations");
		reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException s) {
			s.printStackTrace();
		}
	}
	
	private void addClosestEdge(int idu , Vertex u) {
		int id = -1;
		double min = 1000000;
		Vertex v;
		for(int i = 0 ; i < 321375 ; ++i){
			v = g.getInfoVertex(i);
			double temp = Haversine.distance(u.getLat(), u.getLongi(), v.getLat(), v.getLongi());
			if(temp < min){
				id = i;
				min = temp;
			}
		}
		g.addEdge(idu , id, min);
		g.addEdge(id, idu, min);
	}

	@Override
	public void loadIntersections(){
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(NODES)));
			String s;
			String[] data;
			while((s = in.readLine()) != null){
				data = s.split(",");
				g.addVertex(Integer.parseInt(data[0]), new Vertex(Integer.parseInt(data[0]) , Double.parseDouble(data[1]) , Double.parseDouble(data[2]) , false));
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void loadGraph(){
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(ADJ_LIST)));
			String s;
			String[] data;
			int u , v;
			Vertex fv , fu;
			while((s = in.readLine()) != null){
				if(s.charAt(0) == '#')continue;
				data = s.split(" ");
				u = Integer.parseInt(data[0]);
				fu = g.getInfoVertex(u);
				for(int i = 1 ; i < data.length ; ++i){
					v = Integer.parseInt(data[i]);
					fv = g.getInfoVertex(v);
					Double dist = Haversine.distance(fv.getLat() , fv.getLongi(), fu.getLat(), fu.getLongi());
					g.addEdge(u , v , dist);
					g.addEdge(v , u , dist);
				}
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void createJSON(){
		try {
			PrintWriter outV = new PrintWriter(new FileWriter(VERTEX_JSON));
			PrintWriter outE = new PrintWriter(new FileWriter(EDGE_JSON));
			
			Iterator <Vertex> it = g.allvertex();
			Gson gson = new Gson();
			Vertex v;
			while(it.hasNext()){
				v = it.next();
				outV.println(gson.toJson(v));
				for(Edge e : g.adjEdge(v.getId()))
					outE.println(gson.toJson(e));
			}
			outV.close();			
			outE.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void readJSON(){
 		g = new Graph<>(325000);
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(VERTEX_JSON)));
			String s;
			Gson gson = new Gson();
			while((s = in.readLine()) != null){
				Vertex v = gson.fromJson(s, Vertex.class);
				g.addVertex(v.getId(), v);
			}
			in.close();
			in = new BufferedReader(new FileReader(new File(EDGE_JSON)));
			while((s = in.readLine()) != null){
				Edge e = gson.fromJson(s, Edge.class);
				Double a = (Double) e.idi, b = (Double) e.idf, c =  (Double) e.w;
				g.addEdge(a.intValue(),b.intValue() , c);
			}
			in.close();
		} catch (Exception e) {
			e.getMessage();
			e.printStackTrace();
		}
	}
	
	@Override
	public int V(){
		return g.V();
	}
	
	@Override
	public int E(){
		return g.E();
	}
	
	/**
	 * Dibuja el mapa
	 */
	public void loadMap () {

		MapViewOptions options = new MapViewOptions();
		options.importPlaces();
		
		// Collection - Lista de estaciones y ciclo rutas

		// TODO
		options.setApiKey("AIzaSyAcIO_BBIsU-kiCRypH6-mvLOs_W5iqZ6A"); 
		final Jxmaps mapView = new Jxmaps(options, g.allvertex());

		JFrame frame = new JFrame("Estructuras de Datos 201820 - Prueba JxMaps");

		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(mapView, BorderLayout.CENTER);
		frame.setSize(1800, 1200);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		

	}
	
}