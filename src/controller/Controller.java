package controller;

import api.IManager;
import model.logic.Manager;

public class Controller {
	private static IManager manager = new Manager();

	public static void loadGraph(){
		manager.loadIntersections();
		manager.loadGraph();
	}
	
	public static int V(){
		return manager.V();
	}
	
	public static int E(){
		return manager.E();
	}

	public static void loadStations() {
		manager.loadStations(Manager.STATIONS_Q3_Q4);
	}
	
	public static void createJSON(){
		manager.createJSON();
	}
	
	public static void readJSON(){
		manager.readJSON();
	}

	public static void loadMap() {
		// TODO Auto-generated method stub
		manager.loadMap();
	}
}